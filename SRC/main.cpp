﻿int main() {
    int studentId = 12345;

    try {
        // Получение данных о студенте из базы данных
        Student student = BusinessLogic::getStudentData(studentId);

        // Вывод таблицы оценок студента на экран
        Interface::displayStudentGrades(student);

        // Добавление оценки студенту
        int grade = 5;
        BusinessLogic::addGradeToStudent(studentId, grade);

        // Вывод обновленной таблицы оценок студента на экран
        student = BusinessLogic::getStudentData(studentId);
        Interface::displayStudentGrades(student);
    } catch (const exception& e) {
        cerr << "Ошибка: " << e.what() << endl;
    }

    return 0;
}