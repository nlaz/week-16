#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

class Student {
private:
    int id;
    string name;
    string subject;
    vector<int> grades;

public:
    // Конструктор класса "Студент"
    Student(int id, const string& name, const string& subject, const vector<int>& grades)
        : id(id), name(name), subject(subject), grades(grades) {}

    // Метод получения идентификационного номера студента
    int getId() const {
        return id;
    }

    // Метод получения имени студента
    string getName() const {
        return name;
    }

    // Метод получения названия дисциплины
    string getSubject() const {
        return subject;
    }

    // Метод получения оценок по дисциплине
    vector<int> getGrades() const {
        return grades;
    }

    // Метод добавления оценки по дисциплине
    void addGrade(int grade) {
        grades.push_back(grade);
    }
};

