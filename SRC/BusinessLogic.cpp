class BusinessLogic {
private:
    // Метод чтения данных из текстового файла
    static vector<Student> readDataFromFile(const string& filename) {
        vector<Student> students;
        ifstream file(filename);
        if (file.is_open()) {
            string line;
            while (getline(file, line)) {
                // Разделение строки на отдельные значения
                vector<string> data;
                size_t pos = 0, prev_pos = 0;
                while ((pos = line.find(',', prev_pos)) != string::npos) {
                    data.push_back(line.substr(prev_pos, pos - prev_pos));
                    prev_pos = pos + 1;
                }
                data.push_back(line.substr(prev_pos));

                // Создание объекта класса "Студент"
                int id = stoi(data[0]);
                string name = data[1];
                string subject = data[2];
                vector<int> grades;
                for (int i = 3; i < data.size(); i++) {
                    grades.push_back(stoi(data[i]));
                }
                students.push_back(Student(id, name, subject, grades));
            }
            file.close();
        }
        return students;
    }

public:
    // Метод получения данных о студенте из базы данных
    static Student getStudentData(int studentId) {
        vector<Student> students = readDataFromFile("data.txt");
        for (const Student& student : students) {
            if (student.getId() == studentId) {
                return student;
            }
        }
        throw logic_error("The student with the specified identification number was not found.");
    }

    // Метод добавления оценки студенту
    static void addGradeToStudent(int studentId, int grade) {
        string filename = "data.txt";
        vector<Student> students = readDataFromFile(filename);
        for (Student& student : students) {
            if (student.getId() == studentId) {
                student.addGrade(grade);
                ofstream file(filename);
                if (file.is_open()) {
                    for (const Student& s : students) {
                        file << s.getId() << "," << s.getName() << "," << s.getSubject() << ",";
                        vector<int> grades = s.getGrades();
                        for (int i = 0; i < grades.size(); i++) {
                            file << grades[i];
                            if (i != grades.size() - 1) {
                                file << ",";
                            }
                        }
                        file << endl;
                    }
                    file.close();
                    return;
                } else {
                    throw logic_error("Failed to open file for writing.");
                }
            }
        }
        throw logic_error(" The student with the specified identification number was not found.");
    }
};
